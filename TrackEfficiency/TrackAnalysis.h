#ifndef TrackEfficiency_TrackAnalysis_H
#define TrackEfficiency_TrackAnalysis_H

#include <EventLoop/Algorithm.h>
#include <CommonTools/NtupManager.h>
#include <InDetTrackSelectionTool/InDetTrackSelectionTool.h>

#include <xAODRootAccess/Init.h>
#include <xAODRootAccess/TEvent.h>
#include <xAODEventInfo/EventInfo.h>

#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"

#include <ctime>
#include <chrono>
#include <string>
#include <memory>

class TrackAnalysis : public EL::Algorithm {
 public:
  bool isMC;
  double maxD0;
  double maxZ0st;
  std::string outputName;
  
  TrackAnalysis ();

  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

 private:
  xAOD::TEvent* m_event; //!

  long m_processedEvents; //!
  std::chrono::high_resolution_clock::time_point m_startTime; //!
  
  NtupManager m_ntupManager; //!
  InDet::InDetTrackSelectionTool m_selector; //!

  // Trigger Tools
  //std::unique_ptr<Trig::TrigDecisionTool> m_trigDecisionTool; //!
  //std::unique_ptr<TrigConf::xAODConfigTool> m_trigConfigTool; //!
  Trig::TrigDecisionTool *m_trigDecisionTool; //!
  TrigConf::xAODConfigTool *m_trigConfigTool; //!

  GoodRunsListSelectionTool *m_grl; //!

  // this is needed to distribute the algorithm to the workers
  ClassDef(TrackAnalysis, 1);
};

#endif
