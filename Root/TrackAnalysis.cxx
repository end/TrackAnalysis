//////////////////////////////////////////////////////////////
//              Run II E/p DxAOD2NTUP Converter             //
//                  Emily (Millie) McDonald                 //
//             University of Melbourne, Australia           //
//                       April 2016                         //
//////////////////////////////////////////////////////////////

#include <xAODRootAccess/tools/Message.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <TrackEfficiency/TrackAnalysis.h>
//#include "CLHEP/Geometry/Vector3D.h"

#include <xAODEventInfo/EventInfo.h>
//#include "EventInfo/EventID.h"
//#include "EventInfo/EventType.h"

#include <iostream>
#include <TSystem.h>

#define EL_RETURN_CHECK( EXP )                              \
   do {                                                     \
      if( ! EXP.isSuccess() ) {                             \
         Error( __PRETTY_FUNCTION__,                        \
                XAOD_MESSAGE( "Failed to execute: %s" ),    \
                #EXP );                                     \
         return EL::StatusCode::FAILURE;                    \
      }                                                     \
   } while( false )


// this is needed to distribute the algorithm to the workers
ClassImp(TrackAnalysis)



TrackAnalysis :: TrackAnalysis() : m_selector("TrackSelection") {}



EL::StatusCode TrackAnalysis :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD();
  // let's initialize the algorithm to use the xAODRootAccess package
  xAOD::Init("TrackAnalysis").ignore(); // call before opening first file
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackAnalysis :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.


  // Initialize and configure trigger tools
  m_trigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
  EL_RETURN_CHECK( m_trigConfigTool->initialize() );
  ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_trigConfigTool );
  m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
  EL_RETURN_CHECK(m_trigDecisionTool->setProperty( "ConfigTool", trigConfigHandle ) ); // connect the TrigDecisionTool to the ConfigTool
  EL_RETURN_CHECK(m_trigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision" ) );
  EL_RETURN_CHECK(m_trigDecisionTool->initialize() );
  /*m_trigConfigTool = std::unique_ptr<TrigConf::xAODConfigTool>(new TrigConf::xAODConfigTool("xAODConfigTool"));
  EL_RETURN_CHECK(m_trigConfigTool->initialize());
  ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle(m_trigConfigTool.get());
  m_trigDecisionTool = std::unique_ptr<Trig::TrigDecisionTool>(new Trig::TrigDecisionTool("TrigDecisionTool"));
  EL_RETURN_CHECK(m_trigDecisionTool->setProperty( "ConfigTool", trigConfigHandle)); // connect the TrigDecisionTool to the ConfigTool
  EL_RETURN_CHECK(m_trigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision"));
  EL_RETURN_CHECK(m_trigDecisionTool->initialize());*/

  // GRL
  m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  const char* GRLFilePath = "TrackEfficiency/GRLs/data15_13TeV.periodAllYear_DetStatus-v75-repro20-01_DQDefects-00-02-02_PHYS_StandardModel_MinimuBias2010.xml";
  const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath);
  std::vector<std::string> vecStringGRL;
  vecStringGRL.push_back(fullGRLFilePath);
  EL_RETURN_CHECK(m_grl->setProperty( "GoodRunsListVec", vecStringGRL));
  EL_RETURN_CHECK(m_grl->setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
  EL_RETURN_CHECK(m_grl->initialize());

  m_ntupManager.initialize(outputName, wk()->getOutputFile(outputName));

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackAnalysis :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackAnalysis :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackAnalysis :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  m_event = wk()->xaodEvent();

  m_startTime = std::chrono::high_resolution_clock::now();
  m_processedEvents = 0;

  //EL_RETURN_CHECK(m_selector.setProperty("CutLevel", "MinBias"));
  //EL_RETURN_CHECK(m_selector.setProperty("maxD0", maxD0));
  //EL_RETURN_CHECK(m_selector.setProperty("maxZ0SinTheta", maxZ0st));
  //EL_RETURN_CHECK(m_selector.initialize());

  //isMC = wk()->metaData()->getString("sample_name").find("data") == std::string::npos;

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  ++m_processedEvents;
  // Print out a timing message every 10k events
  if(m_processedEvents % 500 == 0) {
    std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
    auto elapsed = now - m_startTime;
    long long elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
    Info("execute()", "%lu events processed, running at %.3f kHz.", m_processedEvents, double(m_processedEvents)/elapsed_ms);
  }

    const xAOD::EventInfo* eventInfo;
  EL_RETURN_CHECK(m_event->retrieve(eventInfo, "EventInfo"));
  //if( ! m_event->retrieve( eventInfo, "EventInfo").isSuccess() ){
  //  Error("execute()", "Failed to retrieve event info collection. Exiting." );
  //  return EL::StatusCode::FAILURE;
  //}

  //if (eventInfo->getEntries()==0) {
  //  return EL::StatusCode::SUCCESS;
  //}

  const xAOD::TrackParticleContainer* tracks = nullptr;
  EL_RETURN_CHECK(m_event->retrieve( tracks, "InDetTrackParticles"));
  std::cout << tracks->size() << std::endl;
  if (tracks->size() == 0) {
      return EL::StatusCode::SUCCESS; // go to next event
  }

  // check if the event is MC
  bool isMC = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );

  // Only keep triggered events
  /*if (!isMC && !m_trigDecisionTool->isPassed("HLT_noalg_mb_L1MBTS_1")) {*/
    //return EL::StatusCode::SUCCESS;
  /*}*/
  // NOTHING IS PASSING THE TRIGGER ABOVE

  //if(!isMC){ // it's data!
    //if(!m_grl->passRunLB(*eventInfo)){
       //return EL::StatusCode::SUCCESS; // go to next event
    //}
  /*} // end if not MC*/

  // event info
  m_ntupManager.setProperty("runNumber", (int)eventInfo->runNumber()); // runNumber is a uint32_t
  m_ntupManager.setProperty("evtNumber", (int)eventInfo->eventNumber()); // eventNumber is an unsigned long long
  m_ntupManager.setProperty("timeStamp", (int)eventInfo->timeStamp()); //timeStamp is a  uint32_t
  m_ntupManager.setProperty("lumiBlock", (int)eventInfo->lumiBlock()); // lumiBlock is a uint32_t
 // m_ntupManager.setProperty("evtWeight", eventInfo->mcEventWeight()); // mcEventWeight is a float
  m_ntupManager.setProperty("actualInteractionPerXing", eventInfo->actualInteractionsPerCrossing());
  m_ntupManager.setProperty("averageInteractionsPerCrossing", eventInfo->averageInteractionsPerCrossing());


//   int bcid = eventInfo->event_ID()->bunch_crossing_id();
//   if(m_bcid->isInTrain(bcid)){
//       m_aan_dff=m_bcid->distanceFromFront( bcid, Trig::IIBunchCrossingTool::BunchCrossings );
//       ATH_MSG_DEBUG("BCIDs from front of train" << m_aan_dff);
//     }
//     else{
//       m_aan_dff=-20;
//     }
//     m_aan_bc_int=m_bcid->bcIntensity(bcid);
  m_ntupManager.setProperty("bcidNumber", (int)eventInfo->bcid());

  // Consider systematics on:
  //  * Track Selection (MinBias -> LoosePrimary, TightPrimary)
  //  * D0/Z0 Cuts
  //  * Track Multiplicity Cut
  //  * Primary Vertex

  /***********************
   Retrieve primary vertex
  ***********************/
  const xAOD::VertexContainer* vertices = nullptr;
  const xAOD::Vertex* pv = nullptr;
  EL_RETURN_CHECK(m_event->retrieve(vertices, "PrimaryVertices"));

 // Trk::Vertex beamposition(Hep3Vector(0,0,0));
  int NVertex = 0; //Number of primary vertices

  for(const auto& vertex: *vertices) {

    if (vertex->vertexType() != xAOD::VxType::PriVtx) continue;
    NVertex++;

    pv = vertex;
    int nTracksAtVertex = pv->nTrackParticles();
    m_ntupManager.pushProperty("VertexNtracks", nTracksAtVertex);

    int trackCounter = 0;
    /*Looping over tracks associated to the primary vertex*/
    for (auto trkItr : pv->trackParticleLinks()) { //tracksAtVertexElLinks is a vector of element links to track particles associated to a vertex
      if ((*trkItr)->pt() > 500) trackCounter++;
    }

    //Record vertex properties//
    m_ntupManager.pushProperty("VertexNtracks500", trackCounter);
    m_ntupManager.pushProperty("VertexX", pv->position().x());
    m_ntupManager.pushProperty("VertexY", pv->position().y());
    m_ntupManager.pushProperty("VertexZ", pv->position().z());
    m_ntupManager.pushProperty("VertexChi2", pv->chiSquared());
    m_ntupManager.pushProperty("VertexNdof", pv->numberDoF());
  }

  m_ntupManager.setProperty("NVertex", NVertex);

  //beamposition=(*(vertices->begin()))->recVertex();
  //ATH_MSG_DEBUG("PV position " << beamposition.position());

 // const xAOD::TrackParticleContainer* tracks = nullptr;
  //EL_RETURN_CHECK(m_event->retrieve( tracks, "InDetTrackParticles"));

  int ntracks = 0;
  int Index = 0;

  for(const auto& track : *tracks) {

    // Check track originates from primary vertex //
    auto vertex = track->vertex();
    // Check that track originates from a primary vertex
    if(!vertex || vertex->vertexType()!=1) continue;

    if(track->pt() < 500.0) continue;

    //Need some condition here for whether or not extension succeeded //
    using AccInt = SG::AuxElement::ConstAccessor<int>;
    const static AccInt extrapolation("CALO_extrapolation");

    if(!(extrapolation(*track)==1)) continue;

    ntracks++; //Number of tracks with pT > 500 MeV originating from a primary vertx
    Index++;

    m_ntupManager.pushProperty("Index", Index); //Track Index per event

    // Extract Track information //
    double p  = fabs(1./track->qOverP());
    double pt = p*sin(track->theta());

    //set track quantities
    m_ntupManager.pushProperty("TrackP", p); //Track momentum including the z component
    m_ntupManager.pushProperty("TrackPt", pt); //Track momentum in the transverse plane - specifically, the magnitude of the projection of the momentum 3-vector into the x-y plane
    m_ntupManager.pushProperty("TrackQ", track->charge()); //Track electric charge
    m_ntupManager.pushProperty("TrackEta", track->eta());
    m_ntupManager.pushProperty("TrackPhi", track->phi());
    m_ntupManager.pushProperty("TrackD0", track->d0()); //Track transverse impact parameter
    m_ntupManager.pushProperty("TrackZ0", track->z0()); //Track longitudinal impact parameter
    m_ntupManager.pushProperty("TrackTheta", track->theta());
    m_ntupManager.pushProperty("TrackChi2", track->chiSquared()); //Track chi squared
    m_ntupManager.pushProperty("TrackNdf", track->numberDoF()); //Track number of degrees of freedom (Ndf)

    /*const Trk::Track * oTrack = (*trackIter)->originalTrack();
    if (oTrack) {
      m_aan_trackFitAuthor->push_back( oTrack->info().trackFitter() );
      m_aan_trackSeedAuthor->push_back( getSeed(oTrack->info()) );
    } else {
      m_aan_trackFitAuthor->push_back( Trk::TrackInfo::Unknown  );
      m_aan_trackSeedAuthor->push_back( -1 );
    }*/

    uint8_t nPixelHits = 0;
    if (track->summaryValue(nPixelHits, xAOD::numberOfPixelHits)) m_ntupManager.pushProperty("TrackNumberOfPixelHits", nPixelHits);
    uint8_t nSCTHits = 0;
    if (track->summaryValue(nSCTHits, xAOD::numberOfSCTHits)) m_ntupManager.pushProperty("TrackNumberOfSCTHits", nSCTHits);
    uint8_t nTRTHits= 0;
    if (track->summaryValue(nTRTHits, xAOD::numberOfTRTHits)) m_ntupManager.pushProperty("TrackNumberOfTRTHits", nTRTHits);
    uint8_t nHighTRTHits = 0;
    if (track->summaryValue(nHighTRTHits, xAOD::numberOfTRTHighThresholdHits)) m_ntupManager.pushProperty("TrackNumberOfTRTHighThresholdHits", nHighTRTHits);
    uint8_t nPixelHoles = 0;
    if (track->summaryValue(nPixelHoles, xAOD::numberOfPixelHoles)) m_ntupManager.pushProperty("TrackNumberOfPixelHoles", nPixelHoles);
    uint8_t nSCTHoles = 0;
    if (track->summaryValue(nSCTHoles, xAOD::numberOfSCTHoles)) m_ntupManager.pushProperty("TrackNumberOfSCTHoles", nSCTHoles);
    uint8_t nTRTHoles = 0;
    if (track->summaryValue(nTRTHoles, xAOD::numberOfTRTHoles)) m_ntupManager.pushProperty("TrackNumberOfTRTHoles", nTRTHoles);

    //using AccInt = SG::AuxElement::ConstAccessor<int>;
    using AccFlt = SG::AuxElement::ConstAccessor<float>;

    /**********Get extrapolated track info**********/
    const static AccFlt trkEta_PreSamplerB("CALO_trkEta_PreSamplerB");
    const static AccFlt trkPhi_PreSamplerB("CALO_trkPhi_PreSamplerB");

    const static AccFlt trkEta_EMB1("CALO_trkEta_EMB1");
    const static AccFlt trkPhi_EMB1("CALO_trkPhi_EMB1");
    const static AccFlt trkEta_EMB2("CALO_trkEta_EMB2");
    const static AccFlt trkPhi_EMB2("CALO_trkPhi_EMB2");
    const static AccFlt trkEta_EMB3("CALO_trkEta_EMB3");
    const static AccFlt trkPhi_EMB3("CALO_trkPhi_EMB3");

    const static AccFlt trkEta_PreSamplerE("CALO_trkEta_PreSamplerE");
    const static AccFlt trkPhi_PreSamplerE("CALO_trkPhi_PreSamplerE");

    const static AccFlt trkEta_EME1("CALO_trkEta_EME1");
    const static AccFlt trkPhi_EME1("CALO_trkPhi_EME1");
    const static AccFlt trkEta_EME2("CALO_trkEta_EME2");
    const static AccFlt trkPhi_EME2("CALO_trkPhi_EME2");
    const static AccFlt trkEta_EME3("CALO_trkEta_EME3");
    const static AccFlt trkPhi_EME3("CALO_trkPhi_EME3");

    const static AccFlt trkEta_HEC0("CALO_trkEta_HEC0");
    const static AccFlt trkPhi_HEC0("CALO_trkPhi_HEC0");
    const static AccFlt trkEta_HEC1("CALO_trkEta_HEC1");
    const static AccFlt trkPhi_HEC1("CALO_trkPhi_HEC1");
    const static AccFlt trkEta_HEC2("CALO_trkEta_HEC2");
    const static AccFlt trkPhi_HEC2("CALO_trkPhi_HEC2");
    const static AccFlt trkEta_HEC3("CALO_trkEta_HEC3");
    const static AccFlt trkPhi_HEC3("CALO_trkPhi_HEC3");

    const static AccFlt trkEta_TileBar0("CALO_trkEta_TileBar0");
    const static AccFlt trkPhi_TileBar0("CALO_trkPhi_TileBar0");
    const static AccFlt trkEta_TileBar1("CALO_trkEta_TileBar1");
    const static AccFlt trkPhi_TileBar1("CALO_trkPhi_TileBar1");
    const static AccFlt trkEta_TileBar2("CALO_trkEta_TileBar2");
    const static AccFlt trkPhi_TileBar2("CALO_trkPhi_TileBar2");

    const static AccFlt trkEta_TileGap1("CALO_trkEta_TileGap1");
    const static AccFlt trkPhi_TileGap1("CALO_trkPhi_TileGap1");
    const static AccFlt trkEta_TileGap2("CALO_trkEta_TileGap2");
    const static AccFlt trkPhi_TileGap2("CALO_trkPhi_TileGap2");
    const static AccFlt trkEta_TileGap3("CALO_trkEta_TileGap3");
    const static AccFlt trkPhi_TileGap3("CALO_trkPhi_TileGap3");

    const static AccFlt trkEta_TileExt0("CALO_trkEta_TileExt0");
    const static AccFlt trkPhi_TileExt0("CALO_trkPhi_TileExt0");
    const static AccFlt trkEta_TileExt1("CALO_trkEta_TileExt1");
    const static AccFlt trkPhi_TileExt1("CALO_trkPhi_TileExt1");
    const static AccFlt trkEta_TileExt2("CALO_trkEta_TileExt2");
    const static AccFlt trkPhi_TileExt2("CALO_trkPhi_TileExt2");

    //DO I WANT TO IMPOSE RESTRICTIONS ON THE VALUES STORED? NB: IF A TRACK IS NOT EXTRAPOLATED TO THE CALO IT HAS A DEFAULT VALUE OF -999999999.
    m_ntupManager.pushProperty("TrackAtPreSamplerBEta", trkEta_PreSamplerB(*track));
    m_ntupManager.pushProperty("TrackAtEMB1Eta", trkEta_EMB1(*track)); //preserves ordering of xAOD::CaloSample enums
    m_ntupManager.pushProperty("TrackAtEMB2Eta", trkEta_EMB2(*track));
    m_ntupManager.pushProperty("TrackAtEMB3Eta", trkEta_EMB3(*track));
    m_ntupManager.pushProperty("TrackAtPreSamplerEEta", trkEta_PreSamplerE(*track));
    m_ntupManager.pushProperty("TrackAtEME1Eta", trkEta_EME1(*track));
    m_ntupManager.pushProperty("TrackAtEME2Eta", trkEta_EME2(*track));
    m_ntupManager.pushProperty("TrackAtEME3Eta", trkEta_EME3(*track));
    m_ntupManager.pushProperty("TrackAtHEC0Eta", trkEta_HEC0(*track));
    m_ntupManager.pushProperty("TrackAtHEC1Eta", trkEta_HEC1(*track));
    m_ntupManager.pushProperty("TrackAtHEC2Eta", trkEta_HEC2(*track));
    m_ntupManager.pushProperty("TrackAtHEC3Eta", trkEta_HEC3(*track));
    m_ntupManager.pushProperty("TrackAtTileBar0Eta", trkEta_TileBar0(*track));
    m_ntupManager.pushProperty("TrackAtTileBar1Eta", trkEta_TileBar1(*track));
    m_ntupManager.pushProperty("TrackAtTileBar2Eta", trkEta_TileBar2(*track));
    m_ntupManager.pushProperty("TrackAtTileGap1Eta", trkEta_TileGap1(*track));
    m_ntupManager.pushProperty("TrackAtTileGap2Eta", trkEta_TileGap2(*track));
    m_ntupManager.pushProperty("TrackAtTileGap3Eta", trkEta_TileGap3(*track));
    m_ntupManager.pushProperty("TrackAtTileExt0Eta", trkEta_TileExt0(*track));
    m_ntupManager.pushProperty("TrackAtTileExt1Eta", trkEta_TileExt1(*track));
    m_ntupManager.pushProperty("TrackAtTileExt2Eta", trkEta_TileExt2(*track));

    m_ntupManager.pushProperty("TrackAtPreSamplerBPhi", trkPhi_PreSamplerB(*track));
    m_ntupManager.pushProperty("TrackAtEMB1Phi", trkPhi_EMB1(*track)); //preserves ordering of xAOD::CaloSample enums
    m_ntupManager.pushProperty("TrackAtEMB2Phi", trkPhi_EMB2(*track));
    m_ntupManager.pushProperty("TrackAtEMB3Phi", trkPhi_EMB3(*track));
    m_ntupManager.pushProperty("TrackAtPreSamplerEPhi", trkPhi_PreSamplerE(*track));
    m_ntupManager.pushProperty("TrackAtEME1Phi", trkPhi_EME1(*track));
    m_ntupManager.pushProperty("TrackAtEME2Phi", trkPhi_EME2(*track));
    m_ntupManager.pushProperty("TrackAtEME3Phi", trkPhi_EME3(*track));
    m_ntupManager.pushProperty("TrackAtHEC0Phi", trkPhi_HEC0(*track));
    m_ntupManager.pushProperty("TrackAtHEC1Phi", trkPhi_HEC1(*track));
    m_ntupManager.pushProperty("TrackAtHEC2Phi", trkPhi_HEC2(*track));
    m_ntupManager.pushProperty("TrackAtHEC3Phi", trkPhi_HEC3(*track));
    m_ntupManager.pushProperty("TrackAtTileBar0Phi", trkPhi_TileBar0(*track));
    m_ntupManager.pushProperty("TrackAtTileBar1Phi", trkPhi_TileBar1(*track));
    m_ntupManager.pushProperty("TrackAtTileBar2Phi", trkPhi_TileBar2(*track));
    m_ntupManager.pushProperty("TrackAtTileGap1Phi", trkPhi_TileGap1(*track));
    m_ntupManager.pushProperty("TrackAtTileGap2Phi", trkPhi_TileGap2(*track));
    m_ntupManager.pushProperty("TrackAtTileGap3Phi", trkPhi_TileGap3(*track));
    m_ntupManager.pushProperty("TrackAtTileExt0Phi", trkPhi_TileExt0(*track));
    m_ntupManager.pushProperty("TrackAtTileExt1Phi", trkPhi_TileExt1(*track));
    m_ntupManager.pushProperty("TrackAtTileExt2Phi", trkPhi_TileExt2(*track));

    // Get extrapolated track calo cluster info //
    const static AccFlt clEnergy_PreSamplerB_200("CALO_ClusterEnergy_PreSamplerB_200");
    const static AccFlt clEnergy_PreSamplerB_100("CALO_ClusterEnergy_PreSamplerB_100");
    const static AccFlt clEnergy_PreSamplerE_200("CALO_ClusterEnergy_PreSamplerE_200");
    const static AccFlt clEnergy_PreSamplerE_100("CALO_ClusterEnergy_PreSamplerE_100");

    const static AccFlt clEnergy_EMB1_200("CALO_ClusterEnergy_EMB1_200");
    const static AccFlt clEnergy_EMB1_100("CALO_ClusterEnergy_EMB1_100");
    const static AccFlt clEnergy_EMB2_200("CALO_ClusterEnergy_EMB2_200");
    const static AccFlt clEnergy_EMB2_100("CALO_ClusterEnergy_EMB2_100");
    const static AccFlt clEnergy_EMB3_200("CALO_ClusterEnergy_EMB3_200");
    const static AccFlt clEnergy_EMB3_100("CALO_ClusterEnergy_EMB3_100");

    const static AccFlt clEnergy_EME1_200("CALO_ClusterEnergy_EME1_200");
    const static AccFlt clEnergy_EME1_100("CALO_ClusterEnergy_EME1_100");
    const static AccFlt clEnergy_EME2_200("CALO_ClusterEnergy_EME2_200");
    const static AccFlt clEnergy_EME2_100("CALO_ClusterEnergy_EME2_100");
    const static AccFlt clEnergy_EME3_200("CALO_ClusterEnergy_EME3_200");
    const static AccFlt clEnergy_EME3_100("CALO_ClusterEnergy_EME3_100");

    const static AccFlt clEnergy_HEC0_200("CALO_ClusterEnergy_HEC0_200");
    const static AccFlt clEnergy_HEC0_100("CALO_ClusterEnergy_HEC0_100");
    const static AccFlt clEnergy_HEC1_200("CALO_ClusterEnergy_HEC1_200");
    const static AccFlt clEnergy_HEC1_100("CALO_ClusterEnergy_HEC1_100");
    const static AccFlt clEnergy_HEC2_200("CALO_ClusterEnergy_HEC2_200");
    const static AccFlt clEnergy_HEC2_100("CALO_ClusterEnergy_HEC2_100");
    const static AccFlt clEnergy_HEC3_200("CALO_ClusterEnergy_HEC3_200");
    const static AccFlt clEnergy_HEC3_100("CALO_ClusterEnergy_HEC3_100");

    const static AccFlt clEnergy_TileBar0_200("CALO_ClusterEnergy_TileBar0_200");
    const static AccFlt clEnergy_TileBar0_100("CALO_ClusterEnergy_TileBar0_100");
    const static AccFlt clEnergy_TileBar1_200("CALO_ClusterEnergy_TileBar1_200");
    const static AccFlt clEnergy_TileBar1_100("CALO_ClusterEnergy_TileBar1_100");
    const static AccFlt clEnergy_TileBar2_200("CALO_ClusterEnergy_TileBar2_200");
    const static AccFlt clEnergy_TileBar2_100("CALO_ClusterEnergy_TileBar2_100");

    const static AccFlt clEnergy_TileGap1_200("CALO_ClusterEnergy_TileGap1_200");
    const static AccFlt clEnergy_TileGap1_100("CALO_ClusterEnergy_TileGap1_100");
    const static AccFlt clEnergy_TileGap2_200("CALO_ClusterEnergy_TileGap2_200");
    const static AccFlt clEnergy_TileGap2_100("CALO_ClusterEnergy_TileGap2_100");
    const static AccFlt clEnergy_TileGap3_200("CALO_ClusterEnergy_TileGap3_200");
    const static AccFlt clEnergy_TileGap3_100("CALO_ClusterEnergy_TileGap3_100");

    const static AccFlt clEnergy_TileExt0_200("CALO_ClusterEnergy_TileExt0_200");
    const static AccFlt clEnergy_TileExt0_100("CALO_ClusterEnergy_TileExt0_100");
    const static AccFlt clEnergy_TileExt1_200("CALO_ClusterEnergy_TileExt1_200");
    const static AccFlt clEnergy_TileExt1_100("CALO_ClusterEnergy_TileExt1_100");
    const static AccFlt clEnergy_TileExt2_200("CALO_ClusterEnergy_TileExt2_200");
    const static AccFlt clEnergy_TileExt2_100("CALO_ClusterEnergy_TileExt2_100");

    const static AccFlt clEnergy_EM_0_200("CALO_EM_ClusterEnergy_0_200");
    const static AccFlt clEnergy_EM_0_100("CALO_EM_ClusterEnergy_0_100");
    const static AccFlt clEnergy_HAD_0_200("CALO_HAD_ClusterEnergy_0_200");
    const static AccFlt clEnergy_HAD_0_100("CALO_HAD_ClusterEnergy_0_100");
    const static AccFlt clEnergy_Total_0_200("CALO_Total_ClusterEnergy_0_200");
    const static AccFlt clEnergy_Total_0_100("CALO_Total_ClusterEnergy_0_100");

    // DeltaR < 0.2 //
    m_ntupManager.pushProperty("TrackClusterEnergyAtPreSamplerB_0_200", clEnergy_PreSamplerB_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtEMB1_0_200", clEnergy_EMB1_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtEMB2_0_200", clEnergy_EMB2_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtEMB3_0_200", clEnergy_EMB3_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtPreSamplerE_0_200", clEnergy_PreSamplerE_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtEME1_0_200", clEnergy_EME1_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtEME2_0_200", clEnergy_EME2_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtEME3_0_200", clEnergy_EME3_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtHEC0_0_200", clEnergy_HEC0_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtHEC1_0_200", clEnergy_HEC1_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtHEC2_0_200", clEnergy_HEC2_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtHEC3_0_200", clEnergy_HEC3_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileBar0_0_200", clEnergy_TileBar0_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileBar1_0_200", clEnergy_TileBar1_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileBar2_0_200", clEnergy_TileBar2_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileGap1_0_200", clEnergy_TileGap1_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileGap2_0_200", clEnergy_TileGap2_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileGap3_0_200", clEnergy_TileGap3_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileExt0_0_200", clEnergy_TileExt0_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileExt1_0_200", clEnergy_TileExt1_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileExt2_0_200", clEnergy_TileExt2_200(*track));

    m_ntupManager.pushProperty("EM_ClusterEnergy_0_200", clEnergy_EM_0_200(*track));
    m_ntupManager.pushProperty("HAD_ClusterEnergy_0_200", clEnergy_HAD_0_200(*track));
    m_ntupManager.pushProperty("Total_ClusterEnergy_0_200", clEnergy_Total_0_200(*track));

    // DeltaR < 0.1 //
    m_ntupManager.pushProperty("TrackClusterEnergyAtPreSamplerB_0_100", clEnergy_PreSamplerB_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtEMB1_0_100", clEnergy_EMB1_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtEMB2_0_100", clEnergy_EMB2_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtEMB3_0_100", clEnergy_EMB3_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtPreSamplerE_0_100", clEnergy_PreSamplerE_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtEME1_0_100", clEnergy_EME1_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtEME2_0_100", clEnergy_EME2_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtEME3_0_100", clEnergy_EME3_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtHEC0_0_100", clEnergy_HEC0_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtHEC1_0_100", clEnergy_HEC1_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtHEC2_0_100", clEnergy_HEC2_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtHEC3_0_100", clEnergy_HEC3_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileBar0_0_100", clEnergy_TileBar0_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileBar1_0_100", clEnergy_TileBar1_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileBar2_0_100", clEnergy_TileBar2_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileGap1_0_100", clEnergy_TileGap1_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileGap2_0_100", clEnergy_TileGap2_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileGap3_0_100", clEnergy_TileGap3_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileExt0_0_100", clEnergy_TileExt0_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileExt1_0_100", clEnergy_TileExt1_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyAtTileExt2_0_100", clEnergy_TileExt2_100(*track));

    m_ntupManager.pushProperty("EM_ClusterEnergy_0_100", clEnergy_EM_0_100(*track));
    m_ntupManager.pushProperty("HAD_ClusterEnergy_0_100", clEnergy_HAD_0_100(*track));
    m_ntupManager.pushProperty("Total_ClusterEnergy_0_100", clEnergy_Total_0_100(*track));

    // Get extrapolated track calo cluster info at LCW scale //
    const static AccFlt clEnergyLCW_PreSamplerB_200("CALO_ClusterEnergyLCW_PreSamplerB_200");
    const static AccFlt clEnergyLCW_PreSamplerB_100("CALO_ClusterEnergyLCW_PreSamplerB_100");
    const static AccFlt clEnergyLCW_PreSamplerE_200("CALO_ClusterEnergyLCW_PreSamplerE_200");
    const static AccFlt clEnergyLCW_PreSamplerE_100("CALO_ClusterEnergyLCW_PreSamplerE_100");

    const static AccFlt clEnergyLCW_EMB1_200("CALO_ClusterEnergyLCW_EMB1_200");
    const static AccFlt clEnergyLCW_EMB1_100("CALO_ClusterEnergyLCW_EMB1_100");
    const static AccFlt clEnergyLCW_EMB2_200("CALO_ClusterEnergyLCW_EMB2_200");
    const static AccFlt clEnergyLCW_EMB2_100("CALO_ClusterEnergyLCW_EMB2_100");
    const static AccFlt clEnergyLCW_EMB3_200("CALO_ClusterEnergyLCW_EMB3_200");
    const static AccFlt clEnergyLCW_EMB3_100("CALO_ClusterEnergyLCW_EMB3_100");

    const static AccFlt clEnergyLCW_EME1_200("CALO_ClusterEnergyLCW_EME1_200");
    const static AccFlt clEnergyLCW_EME1_100("CALO_ClusterEnergyLCW_EME1_100");
    const static AccFlt clEnergyLCW_EME2_200("CALO_ClusterEnergyLCW_EME2_200");
    const static AccFlt clEnergyLCW_EME2_100("CALO_ClusterEnergyLCW_EME2_100");
    const static AccFlt clEnergyLCW_EME3_200("CALO_ClusterEnergyLCW_EME3_200");
    const static AccFlt clEnergyLCW_EME3_100("CALO_ClusterEnergyLCW_EME3_100");

    const static AccFlt clEnergyLCW_HEC0_200("CALO_ClusterEnergyLCW_HEC0_200");
    const static AccFlt clEnergyLCW_HEC0_100("CALO_ClusterEnergyLCW_HEC0_100");
    const static AccFlt clEnergyLCW_HEC1_200("CALO_ClusterEnergyLCW_HEC1_200");
    const static AccFlt clEnergyLCW_HEC1_100("CALO_ClusterEnergyLCW_HEC1_100");
    const static AccFlt clEnergyLCW_HEC2_200("CALO_ClusterEnergyLCW_HEC2_200");
    const static AccFlt clEnergyLCW_HEC2_100("CALO_ClusterEnergyLCW_HEC2_100");
    const static AccFlt clEnergyLCW_HEC3_200("CALO_ClusterEnergyLCW_HEC3_200");
    const static AccFlt clEnergyLCW_HEC3_100("CALO_ClusterEnergyLCW_HEC3_100");

    const static AccFlt clEnergyLCW_TileBar0_200("CALO_ClusterEnergyLCW_TileBar0_200");
    const static AccFlt clEnergyLCW_TileBar0_100("CALO_ClusterEnergyLCW_TileBar0_100");
    const static AccFlt clEnergyLCW_TileBar1_200("CALO_ClusterEnergyLCW_TileBar1_200");
    const static AccFlt clEnergyLCW_TileBar1_100("CALO_ClusterEnergyLCW_TileBar1_100");
    const static AccFlt clEnergyLCW_TileBar2_200("CALO_ClusterEnergyLCW_TileBar2_200");
    const static AccFlt clEnergyLCW_TileBar2_100("CALO_ClusterEnergyLCW_TileBar2_100");

    const static AccFlt clEnergyLCW_TileGap1_200("CALO_ClusterEnergyLCW_TileGap1_200");
    const static AccFlt clEnergyLCW_TileGap1_100("CALO_ClusterEnergyLCW_TileGap1_100");
    const static AccFlt clEnergyLCW_TileGap2_200("CALO_ClusterEnergyLCW_TileGap2_200");
    const static AccFlt clEnergyLCW_TileGap2_100("CALO_ClusterEnergyLCW_TileGap2_100");
    const static AccFlt clEnergyLCW_TileGap3_200("CALO_ClusterEnergyLCW_TileGap3_200");
    const static AccFlt clEnergyLCW_TileGap3_100("CALO_ClusterEnergyLCW_TileGap3_100");

    const static AccFlt clEnergyLCW_TileExt0_200("CALO_ClusterEnergyLCW_TileExt0_200");
    const static AccFlt clEnergyLCW_TileExt0_100("CALO_ClusterEnergyLCW_TileExt0_100");
    const static AccFlt clEnergyLCW_TileExt1_200("CALO_ClusterEnergyLCW_TileExt1_200");
    const static AccFlt clEnergyLCW_TileExt1_100("CALO_ClusterEnergyLCW_TileExt1_100");
    const static AccFlt clEnergyLCW_TileExt2_200("CALO_ClusterEnergyLCW_TileExt2_200");
    const static AccFlt clEnergyLCW_TileExt2_100("CALO_ClusterEnergyLCW_TileExt2_100");

    const static AccFlt clEnergyLCW_EM_0_200("CALO_EM_ClusterEnergyLCW_0_200");
    const static AccFlt clEnergyLCW_EM_0_100("CALO_EM_ClusterEnergyLCW_0_100");
    const static AccFlt clEnergyLCW_HAD_0_200("CALO_HAD_ClusterEnergyLCW_0_200");
    const static AccFlt clEnergyLCW_HAD_0_100("CALO_HAD_ClusterEnergyLCW_0_100");
    const static AccFlt clEnergyLCW_Total_0_200("CALO_Total_ClusterEnergyLCW_0_200");
    const static AccFlt clEnergyLCW_Total_0_100("CALO_Total_ClusterEnergyLCW_0_100");

    // DeltaR < 0.2 //
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtPreSamplerB_0_200", clEnergyLCW_PreSamplerB_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtEMB1_0_200", clEnergyLCW_EMB1_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtEMB2_0_200", clEnergyLCW_EMB2_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtEMB3_0_200", clEnergyLCW_EMB3_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtPreSamplerE_0_200", clEnergyLCW_PreSamplerE_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtEME1_0_200", clEnergyLCW_EME1_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtEME2_0_200", clEnergyLCW_EME2_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtEME3_0_200", clEnergyLCW_EME3_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtHEC0_0_200", clEnergyLCW_HEC0_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtHEC1_0_200", clEnergyLCW_HEC1_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtHEC2_0_200", clEnergyLCW_HEC2_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtHEC3_0_200", clEnergyLCW_HEC3_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileBar0_0_200", clEnergyLCW_TileBar0_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileBar1_0_200", clEnergyLCW_TileBar1_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileBar2_0_200", clEnergyLCW_TileBar2_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileGap1_0_200", clEnergyLCW_TileGap1_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileGap2_0_200", clEnergyLCW_TileGap2_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileGap3_0_200", clEnergyLCW_TileGap3_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileExt0_0_200", clEnergyLCW_TileExt0_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileExt1_0_200", clEnergyLCW_TileExt1_200(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileExt2_0_200", clEnergyLCW_TileExt2_200(*track));

    m_ntupManager.pushProperty("EM_ClusterEnergyLCW_0_200", clEnergyLCW_EM_0_200(*track));
    m_ntupManager.pushProperty("HAD_ClusterEnergyLCW_0_200", clEnergyLCW_HAD_0_200(*track));
    m_ntupManager.pushProperty("Total_ClusterEnergyLCW_0_200", clEnergyLCW_Total_0_200(*track));

    // DeltaR < 0.1 //
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtPreSamplerB_0_100", clEnergyLCW_PreSamplerB_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtEMB1_0_100", clEnergyLCW_EMB1_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtEMB2_0_100", clEnergyLCW_EMB2_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtEMB3_0_100", clEnergyLCW_EMB3_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtPreSamplerE_0_100", clEnergyLCW_PreSamplerE_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtEME1_0_100", clEnergyLCW_EME1_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtEME2_0_100", clEnergyLCW_EME2_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtEME3_0_100", clEnergyLCW_EME3_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtHEC0_0_100", clEnergyLCW_HEC0_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtHEC1_0_100", clEnergyLCW_HEC1_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtHEC2_0_100", clEnergyLCW_HEC2_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtHEC3_0_100", clEnergyLCW_HEC3_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileBar0_0_100", clEnergyLCW_TileBar0_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileBar1_0_100", clEnergyLCW_TileBar1_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileBar2_0_100", clEnergyLCW_TileBar2_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileGap1_0_100", clEnergyLCW_TileGap1_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileGap2_0_100", clEnergyLCW_TileGap2_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileGap3_0_100", clEnergyLCW_TileGap3_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileExt0_0_100", clEnergyLCW_TileExt0_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileExt1_0_100", clEnergyLCW_TileExt1_100(*track));
    m_ntupManager.pushProperty("TrackClusterEnergyLCWAtTileExt2_0_100", clEnergyLCW_TileExt2_100(*track));

    m_ntupManager.pushProperty("EM_ClusterEnergyLCW_0_100", clEnergyLCW_EM_0_100(*track));
    m_ntupManager.pushProperty("HAD_ClusterEnergyLCW_0_100", clEnergyLCW_HAD_0_100(*track));
    m_ntupManager.pushProperty("Total_ClusterEnergyLCW_0_100", clEnergyLCW_Total_0_100(*track));

    // Get extrapolated track cell info //
    const static AccFlt cellEnergy_PreSamplerB_200("CALO_CellEnergy_PreSamplerB_200");
    const static AccFlt cellEnergy_PreSamplerB_100("CALO_CellEnergy_PreSamplerB_100");
    const static AccFlt cellEnergy_PreSamplerE_200("CALO_CellEnergy_PreSamplerE_200");
    const static AccFlt cellEnergy_PreSamplerE_100("CALO_CellEnergy_PreSamplerE_100");

    const static AccFlt cellEnergy_EMB1_200("CALO_CellEnergy_EMB1_200");
    const static AccFlt cellEnergy_EMB1_100("CALO_CellEnergy_EMB1_100");
    const static AccFlt cellEnergy_EMB2_200("CALO_CellEnergy_EMB2_200");
    const static AccFlt cellEnergy_EMB2_100("CALO_CellEnergy_EMB2_100");
    const static AccFlt cellEnergy_EMB3_200("CALO_CellEnergy_EMB3_200");
    const static AccFlt cellEnergy_EMB3_100("CALO_CellEnergy_EMB3_100");

    const static AccFlt cellEnergy_EME1_200("CALO_CellEnergy_EME1_200");
    const static AccFlt cellEnergy_EME1_100("CALO_CellEnergy_EME1_100");
    const static AccFlt cellEnergy_EME2_200("CALO_CellEnergy_EME2_200");
    const static AccFlt cellEnergy_EME2_100("CALO_CellEnergy_EME2_100");
    const static AccFlt cellEnergy_EME3_200("CALO_CellEnergy_EME3_200");
    const static AccFlt cellEnergy_EME3_100("CALO_CellEnergy_EME3_100");

    const static AccFlt cellEnergy_HEC0_200("CALO_CellEnergy_HEC0_200");
    const static AccFlt cellEnergy_HEC0_100("CALO_CellEnergy_HEC0_100");
    const static AccFlt cellEnergy_HEC1_200("CALO_CellEnergy_HEC1_200");
    const static AccFlt cellEnergy_HEC1_100("CALO_CellEnergy_HEC1_100");
    const static AccFlt cellEnergy_HEC2_200("CALO_CellEnergy_HEC2_200");
    const static AccFlt cellEnergy_HEC2_100("CALO_CellEnergy_HEC2_100");
    const static AccFlt cellEnergy_HEC3_200("CALO_CellEnergy_HEC3_200");
    const static AccFlt cellEnergy_HEC3_100("CALO_CellEnergy_HEC3_100");

    const static AccFlt cellEnergy_TileBar0_200("CALO_CellEnergy_TileBar0_200");
    const static AccFlt cellEnergy_TileBar0_100("CALO_CellEnergy_TileBar0_100");
    const static AccFlt cellEnergy_TileBar1_200("CALO_CellEnergy_TileBar1_200");
    const static AccFlt cellEnergy_TileBar1_100("CALO_CellEnergy_TileBar1_100");
    const static AccFlt cellEnergy_TileBar2_200("CALO_CellEnergy_TileBar2_200");
    const static AccFlt cellEnergy_TileBar2_100("CALO_CellEnergy_TileBar2_100");

    const static AccFlt cellEnergy_TileGap1_200("CALO_CellEnergy_TileGap1_200");
    const static AccFlt cellEnergy_TileGap1_100("CALO_CellEnergy_TileGap1_100");
    const static AccFlt cellEnergy_TileGap2_200("CALO_CellEnergy_TileGap2_200");
    const static AccFlt cellEnergy_TileGap2_100("CALO_CellEnergy_TileGap2_100");
    const static AccFlt cellEnergy_TileGap3_200("CALO_CellEnergy_TileGap3_200");
    const static AccFlt cellEnergy_TileGap3_100("CALO_CellEnergy_TileGap3_100");

    const static AccFlt cellEnergy_TileExt0_200("CALO_CellEnergy_TileExt0_200");
    const static AccFlt cellEnergy_TileExt0_100("CALO_CellEnergy_TileExt0_100");
    const static AccFlt cellEnergy_TileExt1_200("CALO_CellEnergy_TileExt1_200");
    const static AccFlt cellEnergy_TileExt1_100("CALO_CellEnergy_TileExt1_100");
    const static AccFlt cellEnergy_TileExt2_200("CALO_CellEnergy_TileExt2_200");
    const static AccFlt cellEnergy_TileExt2_100("CALO_CellEnergy_TileExt2_100");

    const static AccFlt cellEnergy_EM_0_200("CALO_EM_CellEnergy_0_200");
    const static AccFlt cellEnergy_EM_0_100("CALO_EM_CellEnergy_0_100");
    const static AccFlt cellEnergy_HAD_0_200("CALO_HAD_CellEnergy_0_200");
    const static AccFlt cellEnergy_HAD_0_100("CALO_HAD_CellEnergy_0_100");
    const static AccFlt cellEnergy_Total_0_200("CALO_Total_CellEnergy_0_200");
    const static AccFlt cellEnergy_Total_0_100("CALO_Total_CellEnergy_0_100");

    // DeltaR < 0.2 //
    m_ntupManager.pushProperty("TrackCellEnergyAtPreSamplerB_0_200", cellEnergy_PreSamplerB_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtEMB1_0_200", cellEnergy_EMB1_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtEMB2_0_200", cellEnergy_EMB2_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtEMB3_0_200", cellEnergy_EMB3_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtPreSamplerE_0_200", cellEnergy_PreSamplerE_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtEME1_0_200", cellEnergy_EME1_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtEME2_0_200", cellEnergy_EME2_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtEME3_0_200", cellEnergy_EME3_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtHEC0_0_200", cellEnergy_HEC0_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtHEC1_0_200", cellEnergy_HEC1_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtHEC2_0_200", cellEnergy_HEC2_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtHEC3_0_200", cellEnergy_HEC3_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileBar0_0_200", cellEnergy_TileBar0_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileBar1_0_200", cellEnergy_TileBar1_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileBar2_0_200", cellEnergy_TileBar2_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileGap1_0_200", cellEnergy_TileGap1_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileGap2_0_200", cellEnergy_TileGap2_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileGap3_0_200", cellEnergy_TileGap3_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileExt0_0_200", cellEnergy_TileExt0_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileExt1_0_200", cellEnergy_TileExt1_200(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileExt2_0_200", cellEnergy_TileExt2_200(*track));

    m_ntupManager.pushProperty("EM_CellEnergy_0_200", cellEnergy_EM_0_200(*track));
    m_ntupManager.pushProperty("HAD_CellEnergy_0_200", cellEnergy_HAD_0_200(*track));
    m_ntupManager.pushProperty("Total_CellEnergy_0_200", cellEnergy_Total_0_200(*track));

    // DeltaR < 0.1 //
    m_ntupManager.pushProperty("TrackCellEnergyAtPreSamplerB_0_100", cellEnergy_PreSamplerB_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtEMB1_0_100", cellEnergy_EMB1_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtEMB2_0_100", cellEnergy_EMB2_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtEMB3_0_100", cellEnergy_EMB3_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtPreSamplerE_0_100", cellEnergy_PreSamplerE_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtEME1_0_100", cellEnergy_EME1_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtEME2_0_100", cellEnergy_EME2_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtEME3_0_100", cellEnergy_EME3_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtHEC0_0_100", cellEnergy_HEC0_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtHEC1_0_100", cellEnergy_HEC1_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtHEC2_0_100", cellEnergy_HEC2_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtHEC3_0_100", cellEnergy_HEC3_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileBar0_0_100", cellEnergy_TileBar0_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileBar1_0_100", cellEnergy_TileBar1_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileBar2_0_100", cellEnergy_TileBar2_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileGap1_0_100", cellEnergy_TileGap1_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileGap2_0_100", cellEnergy_TileGap2_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileGap3_0_100", cellEnergy_TileGap3_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileExt0_0_100", cellEnergy_TileExt0_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileExt1_0_100", cellEnergy_TileExt1_100(*track));
    m_ntupManager.pushProperty("TrackCellEnergyAtTileExt2_0_100", cellEnergy_TileExt2_100(*track));

    m_ntupManager.pushProperty("EM_CellEnergy_0_100", cellEnergy_EM_0_100(*track));
    m_ntupManager.pushProperty("HAD_CellEnergy_0_100", cellEnergy_HAD_0_100(*track));
    m_ntupManager.pushProperty("Total_CellEnergy_0_100", cellEnergy_Total_0_100(*track));

  } // end loop over tracks

  m_ntupManager.setProperty("ntracks", ntracks);

  /*for(const auto& vertex: *vertices) {
    if (vertex->vertexType() == xAOD::VxType::PriVtx) {
      pv = vertex;
      break;
    }
  }

  const xAOD::TrackParticleContainer* tracks = nullptr;
  EL_RETURN_CHECK(m_event->retrieve( tracks, "InDetTrackParticles"));

  // Require a low track-multiplicity event
  int ntracks = 0;

  for(const auto& track : *tracks) {

    if(track->pt() < 500.0) continue;
    ntracks++;
  }

  m_ntupManager.setProperty("ntracks", ntracks);

  using AccInt = SG::AuxElement::ConstAccessor<int>;
  using AccFlt = SG::AuxElement::ConstAccessor<float>;

  const static AccInt type("MBTS_cellType");
  const static AccInt module("MBTS_cellModule");
  const static AccInt channel("MBTS_cellChannel");

  const static AccFlt charge("MBTS_cellEnergy");
  const static AccFlt eta("MBTS_eta");
  const static AccFlt phi("MBTS_phi");

  for(const auto& track : *tracks) {
    if (pv){
      if (!m_selector.accept(*track, pv)) continue;
    }
    else {
      if (!m_selector.accept(*track)) continue;
    }
    if (charge(*track) < -200.0) continue; // Didn't extrapolate to mbts

    m_ntupManager.pushProperty("MBTSType", type(*track));
    m_ntupManager.pushProperty("MBTSChannel", channel(*track));
    m_ntupManager.pushProperty("MBTSModule", module(*track));

    m_ntupManager.pushProperty("MBTSCharge", charge(*track));
    m_ntupManager.pushProperty("MBTSEta", eta(*track));
    m_ntupManager.pushProperty("MBTSPhi", phi(*track));
  }*/

  m_ntupManager.fill();
  m_ntupManager.clear();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackAnalysis :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TrackAnalysis :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
